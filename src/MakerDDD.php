<?php


namespace Pag\MakerPagDdd;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use function dirname;

class MakerDDD extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
    }
    
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
